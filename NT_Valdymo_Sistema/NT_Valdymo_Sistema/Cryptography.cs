﻿using System;
using System.Text;

namespace NT_Valdymo_Sistema
{
    public static class Cryptography
    {
        public static string Hash(string value)
        {
            return Convert.ToBase64String(
                System.Security.Cryptography.SHA256.Create()
                .ComputeHash(Encoding.UTF8.GetBytes(value))
                );
        }
    }
}