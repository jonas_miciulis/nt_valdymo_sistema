﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NT_Valdymo_Sistema.Models;
using System.Web.Mvc;
using System.Data.Entity;
using System.Net.Mail;
using System.Net;
using System.Web.Security;

namespace NT_Valdymo_Sistema.Controllers
{
    public class SpValuationController : Controller
    {
        // GET: SpValuation
        public ActionResult SpecialistMain()
        {
            return View();
        }

        [HttpGet]
        public ActionResult SpRequestAndReview()
        {
            using (REDatabaseEntities dc = new REDatabaseEntities())
            {
                var reviews = new List<Review>();
                reviews = dc.Reviews.ToList();

                var reviewsAddresses = new List<String>();
                var newReviewsList = new List<Review>();
                var declarations = new List<Declaration>(); // deklaruoto turto sąrašas

                foreach (var rev in reviews)
                {
                    declarations.Add(dc.Declaration.Where(a => a.DeclarationID == rev.FKDeclarationID).First());
                }

                var houses = new List<House>(); // deklaruotų namų sąrašas

                foreach (var d in declarations) // susidedam deklaruotų namų info ir deklaracijų ID
                {
                    houses.Add(dc.House.Where(a => a.HouseID == d.HouseID).First());
                    newReviewsList.Add(dc.Reviews.Where(a => a.FKDeclarationID == d.DeclarationID).First());
                }
                for (int i = 0; i < houses.Count; i++) // į deklaracijų dictionary susidedam informaciją iš deklaruotų namų
                {
                    reviewsAddresses.Add(houses[i].Settlement + ", " + houses[i].Street + " " + houses[i].HouseNr);
                }
                ViewBag.reviewsAddress = reviewsAddresses;
                ViewBag.reviews = newReviewsList;
            }
            return View();
        }

        [HttpGet]
        public ActionResult SpValuationForm(int? id)
        {
            REDatabaseEntities db = new REDatabaseEntities();
            if (id == null)
            {
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.BadRequest);
            }
            var review = db.Reviews.Find(id);
            if (review != null)
            {
                var FKDeclarationID = review.FKDeclarationID;
                var reviewAddres = "";
                var reviewType = "";
                var flat = new Flat();
                var house = new House();
                var garage = new Garage();
                var plot = new Plot();
                var declaration = db.Declaration.Find(FKDeclarationID);
                if (declaration.FlatID != null)
                {
                    var flatID = (int)declaration.FlatID;
                    flat = db.Flat.Find(flatID);
                    reviewType = "Butas";
                    reviewAddres = flat.Settlement + ", " + flat.Street + " " + flat.HouseNr;
                }
                else if (declaration.HouseID != null)
                {
                    var houseID = (int)declaration.HouseID;
                    house = db.House.Find(houseID);
                    reviewType = "Namas, sodo namas, sodyba";
                    reviewAddres = house.Settlement + ", " + house.Street + " " + house.HouseNr;
                }
                else if (declaration.GarageID != null)
                {
                    var garageID = (int)declaration.GarageID;
                    garage = db.Garage.Find(garageID);
                    reviewType = "Garažas";
                    reviewAddres = garage.Settlement + ", " + garage.Street;
                }
                else if (declaration.PlotID != null)
                {
                    var plotID = (int)declaration.PlotID;
                    plot = db.Plot.Find(plotID);
                    reviewType = "Sklypas";
                    reviewAddres = plot.Settlement + ", " + plot.Street;
                }

                if (review == null)
                {
                    return HttpNotFound();
                }
                ViewBag.reviewUserID = review.ClientID;
                ViewBag.house = house;
                ViewBag.reviewType = reviewType;
                ViewBag.reviewAddres = reviewAddres;
            }
            return View(review);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]

        public ActionResult SpValuationForm(Review review)
        {
            bool successStatus = false;
            string message = "";
            if (ModelState.IsValid)
            {
                using (REDatabaseEntities dc = new REDatabaseEntities()) { 
                    var userID = Convert.ToInt32(Session["userID"]);
                    var updatedReview = new Review();
                    updatedReview.ReviewID = review.ReviewID;
                    updatedReview.RequestDate = review.RequestDate;
                    updatedReview.Sum = review.Sum;
                    updatedReview.ReviewText = review.ReviewText;
                    updatedReview.ReviewDate = System.DateTime.Now;
                    updatedReview.EditDate = System.DateTime.Now;
                    updatedReview.SendToEmail = review.SendToEmail;
                    updatedReview.Status = "Vertinimas atliktas";
                    updatedReview.ClientID = review.ClientID;
                    updatedReview.SpecialistID = userID;

                    updatedReview.FKDeclarationID = review.FKDeclarationID;

                //    dc.Reviews.Attach(updatedReview);
                    dc.Entry(updatedReview).State = EntityState.Modified;
                    dc.SaveChanges();

                    var sendToEmail = (bool)review.SendToEmail;
                    if (sendToEmail)
                    {
                        var clientID = review.ClientID;
                        var client = dc.User.Find(clientID);
                        var clientEmail = (string) client.Email;
                        sendMessage(clientEmail);

                    }
                }
                message = "Sėkmingai išsiuntėtę įvertinimą!";
                successStatus = true;
            }

            ViewBag.Message = message;
            ViewBag.Status = successStatus;
            return View(review);
        }

        public void sendMessage(string receiverEmail)
        {
            var fromEmail = new MailAddress("psantsistema@gmail.com", "NT Sistema");
            var toEmail = new MailAddress(receiverEmail);
            var fromEmailPassword = "passwordas";
            string subject = "Buvo atliktas įvertinimas!";
            string body = "<br/><br/> Buvo įvertintas jūsų turtas! Užsukite į NT sistemą, kad peržiūrėtumėt įvertinimą";
            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(fromEmail.Address, fromEmailPassword)
            };

            using (var message = new MailMessage(fromEmail, toEmail)
            {
                Subject = subject,
                Body = body,
                IsBodyHtml = true
            })
                smtp.Send(message);
        }

    }
}