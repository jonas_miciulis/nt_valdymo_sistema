﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NT_Valdymo_Sistema.Models;
using System.Web.Mvc;

namespace NT_Valdymo_Sistema.Controllers
{
    public class ClValuationController : Controller
    {
        // GET: ClValuation
        [HttpGet]
        public ActionResult ClientMain()
        {
            return View();
        }

        [HttpGet]
        public ActionResult ClFillingForm()
        {
            Dictionary<string, int> RETypesDictionary = new Dictionary<string, int>();

            RETypesDictionary.Add("Butas", 1);
            RETypesDictionary.Add("Namas, sodyba, sodo namas", 2);
            RETypesDictionary.Add("Garažas", 3);
            RETypesDictionary.Add("Sklypas", 4);

            ViewBag.RETyperDictionary = RETypesDictionary;

            Dictionary<string, int> declarartionDictionary = new Dictionary<string, int>();
            using (REDatabaseEntities dc = new REDatabaseEntities())
            {
                var declarations = new List<Declaration>(); // deklaruoto turto sąrašas
                var declarationsIdList = new List<int>(); // deklaruoto turto id sąrašas
                var houses = new List<House>(); // deklaruotų namų sąrašas
                var userID = Convert.ToInt32(Session["userID"]);
                declarations = dc.Declaration.Where(a => a.UserID == userID).ToList(); // pasiimam vartotojo deklaracijas
                foreach (var d in declarations) // susidedam deklaruotų namų info ir deklaracijų ID
                {
                    houses.Add(dc.House.Where(a => a.HouseID == d.HouseID).First());
                    declarationsIdList.Add(d.DeclarationID);
                }

                for (int i = 0; i < houses.Count; i++) // į deklaracijų dictionary susidedam informaciją iš deklaruotų namų
                {
                    declarartionDictionary.Add(houses[i].Settlement + ", " + houses[i].Street + " " + houses[i].HouseNr, declarationsIdList[i]);
                }
                if (declarartionDictionary.Count == 0)
                {
                    declarartionDictionary.Add("", -1);
                }
                ViewBag.declarartionDictionary = declarartionDictionary;
            }

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ClFillingForm(ClFillingForm fillingForm)
        {
            bool successStatus = false;
            string message = "";

            if (ModelState.IsValid)
            {
                using (REDatabaseEntities dc = new REDatabaseEntities())
                {
                    var userID = Convert.ToInt32(Session["userID"]);
                    var review = new Review();
                    review.RequestDate = System.DateTime.Now;
                    review.SendToEmail = fillingForm.sendeMessageToEmail;
                    review.Status = "Pateiktas";
                    review.ClientID = userID;
                    review.REType = (string) fillingForm.selectType;
                    review.Sum = 0;
                    string SelectedValue = fillingForm.selectDeclaration;
                    if (!SelectedValue.Equals("-1")) // jei vartotojas turi deklaruoto turto
                    {
                        int fkKey = Int32.Parse(SelectedValue);
                        review.FKDeclarationID = fkKey;

                        var existingReviews = new List<Review>();
                        existingReviews = dc.Reviews.Where(a => a.FKDeclarationID == fkKey).ToList();
                        if (existingReviews.Count == 0) // jei dar neišsiųsta užklausa deklaruotui turtui
                        {
                            dc.Reviews.Add(review);
                            dc.SaveChanges();
                            message = "Sėkmingai išsiuntėtę užklausą!";
                            successStatus = true;
                        }
                        else
                        {
                            message = "Jūs jau pateikėte šį turtą įvertinimui";
                            successStatus = false;
                        }
                    }
                    else
                    {
                        message = "Deja, Jūs neturite deklaruoto turto";
                        successStatus = false;
                    }
                }
            }

            ViewBag.Message = message;
            ViewBag.Status = successStatus;
            return View(fillingForm);
        }


        [HttpGet]
        public ActionResult ClReviews()
        {
            var reviewsAddresses = new List<String>();
            using (REDatabaseEntities dc = new REDatabaseEntities())
            {
                var reviews = new List<Review>();
                var newReviewsList = new List<Review>();
                var declarations = new List<Declaration>(); // deklaruoto turto sąrašas
                var userID = Convert.ToInt32(Session["userID"]);
                reviews = dc.Reviews.Where(a => a.ClientID == userID).ToList();

                foreach (var rev in reviews)
                {
                    declarations.Add(dc.Declaration.Where(a => a.DeclarationID == rev.FKDeclarationID).First());
                }

                var houses = new List<House>(); // deklaruotų namų sąrašas

                foreach (var d in declarations) // susidedam deklaruotų namų info ir deklaracijų ID
                {
                    houses.Add(dc.House.Where(a => a.HouseID == d.HouseID).First());
                    newReviewsList.Add(dc.Reviews.Where(a => a.FKDeclarationID == d.DeclarationID).First());
                }
                for (int i = 0; i < houses.Count; i++) // į deklaracijų dictionary susidedam informaciją iš deklaruotų namų
                {
                    reviewsAddresses.Add(houses[i].Settlement + ", " + houses[i].Street + " " + houses[i].HouseNr);
                }
                ViewBag.reviewsAddress = reviewsAddresses;
                ViewBag.reviews = newReviewsList;
            }

            return View();
        }

        [HttpGet]
        public ActionResult ClParticularReview(int? id)
        {
            REDatabaseEntities db = new REDatabaseEntities();
            if (id == null)
            {
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.BadRequest);
            }
            var review = db.Reviews.Find(id);
            if (review != null)
            {
                var FKDeclarationID = review.FKDeclarationID;
                var reviewAddres = "";
                var reviewType = "";
                var flat = new Flat();
                var house = new House();
                var garage = new Garage();
                var plot = new Plot();
                var declaration = db.Declaration.Find(FKDeclarationID);
                if (declaration.FlatID != null)
                {
                    var flatID = (int)declaration.FlatID;
                    flat = db.Flat.Find(flatID);
                    reviewType = "Butas";
                    reviewAddres = flat.Settlement + ", " + flat.Street + " " + flat.HouseNr;
                }
                else if (declaration.HouseID != null)
                {
                    var houseID = (int)declaration.HouseID;
                    house = db.House.Find(houseID);
                    reviewType = "Namas, sodo namas, sodyba";
                    reviewAddres = house.Settlement + ", " + house.Street + " " + house.HouseNr;
                }
                else if (declaration.GarageID != null)
                {
                    var garageID = (int)declaration.GarageID;
                    garage = db.Garage.Find(garageID);
                    reviewType = "Garažas";
                    reviewAddres = garage.Settlement + ", " + garage.Street;
                }
                else if (declaration.PlotID != null)
                {
                    var plotID = (int)declaration.PlotID;
                    plot = db.Plot.Find(plotID);
                    reviewType = "Sklypas";
                    reviewAddres = plot.Settlement + ", " + plot.Street;
                }

                if (review == null)
                {
                    return HttpNotFound();
                }
                ViewBag.reviewUserID = review.ClientID;
                ViewBag.house = house;
                ViewBag.reviewType = reviewType;
                ViewBag.reviewAddres = reviewAddres;
            }
            return View(review);
        }
    }
}