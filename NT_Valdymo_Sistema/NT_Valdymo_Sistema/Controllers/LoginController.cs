﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NT_Valdymo_Sistema.Models;
using System.Web.Security;

namespace NT_Valdymo_Sistema.Controllers
{
    public class LoginController : Controller
    {
        // Registracijos veiksmas
        [HttpGet]
        public ActionResult Registration()
        {
            return View();
        }

        // Registracijos POST (įrašymas į bazę)

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Registration(User user)
        {
            bool Status = false;
            string message = "";

            // Modelio validacija

            if (ModelState.IsValid)
            {
                #region // El paštas jau yra
                var isEmailExit = isEmailExist(user.Email);
                if (isEmailExit)
                {
                    //    ModelState.AddModelError("El paštas egzistuoja", "Toks el. paštas jau užregistruotas!");
                    message = "El. paštas " + user.Email + " jau yra užregistruotas";
                    ViewBag.Message = message;
                    ViewBag.Status = Status;
                    return View(user);
                }
                #endregion
                #region // Slaptažodžio šifravimas (hash)
                user.Password = Cryptography.Hash(user.Password);
                user.ConfirmPassword = Cryptography.Hash(user.ConfirmPassword);
                #endregion

                #region // Išsaugojimas į duomenų bazę
                using (REDatabaseEntities dc = new REDatabaseEntities())
                {
                    user.RegistrationDate = System.DateTime.Now;
                    user.EditingDate = System.DateTime.Now;
                    user.UserType = "Klientas";
                    user.RegistrationState = "Nepatvirtinta";

                    dc.User.Add(user);
                    dc.SaveChanges();

                    //message = "Sėkmingai užsiregistravote!";
                    Status = true;
                    return RedirectToAction("Login", "Login");
                }
                #endregion
            }
            else
            {
                message = "Neteisinga užklausa";
            }

            ViewBag.Message = message;
            ViewBag.Status = Status;
            return View(user);
        }

        //Prisijungimas
        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }


        //Login POST (įrašymas į bazę)
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(UserLogin login, string ReturnUrl = "")
        {
            string message = "";
            using (REDatabaseEntities dc = new REDatabaseEntities())
            {
                var v = dc.User.Where(a => a.Email == login.email).FirstOrDefault();
                if (v != null)
                {
                    if (string.Compare(Cryptography.Hash(login.password), v.Password) == 0)
                    {
                        int timeout = login.rememberMe ? 525600 : 90;
                        var ticket = new FormsAuthenticationTicket(login.email, login.rememberMe, timeout);
                        string encrypted = FormsAuthentication.Encrypt(ticket);
                        var cookie = new HttpCookie(FormsAuthentication.FormsCookieName, encrypted);
                        cookie.Expires = DateTime.Now.AddMinutes(timeout);
                        cookie.HttpOnly = true;
                        Response.Cookies.Add(cookie);
                        Session["userType"] = v.UserType; // Įsirašom į session vartotojo tipą
                        Session["userID"] = v.UserID;
                        Session["userName"] = v.Name;
                        if (Url.IsLocalUrl(ReturnUrl))
                        {
                            return Redirect(ReturnUrl);
                        }
                        else
                        {
                            if (v.UserType == "Klientas")
                            {
                                return RedirectToAction("Subsystem", "Declaration");
                            }
                            else if (v.UserType == "Priziuretojas")
                            {
                                return RedirectToAction("AccountsDisplay", "Accounts");
                            }
                            else if (v.UserType == "Buhalteris")
                            {
                                return RedirectToAction("AccountantMainWindow", "Report");
                            }
                            else if (v.UserType == "Specialistas")
                            {
                                return RedirectToAction("SpecialistMain", "SpValuation");
                            }
                            else if (v.UserType == "Administratorius")
                            {
                                return RedirectToAction("MainAdministrator", "Administrator");
                            }
                        }
                    }
                    else
                    {
                        message = "Įvyko klaida";
                    }

                }
                else
                {
                    message = "Įvyko klaida";
                }
            }

            ViewBag.Message = message;
            return View();
        }

        // Atsijungimas
        [Authorize]
        //  [HttpPost]
        [HttpGet]
        public ActionResult Logout()
        {
            Session["userType"] = null; // Išvalom session
            FormsAuthentication.SignOut();
            return RedirectToAction("Login", "Login");
        }


        [NonAction]
        public bool isEmailExist(string email)
        {
            using (REDatabaseEntities dc = new REDatabaseEntities())
            {
                var v = dc.User.Where(a => a.Email == email).FirstOrDefault();
                return v != null;
            }
        }
    }
}