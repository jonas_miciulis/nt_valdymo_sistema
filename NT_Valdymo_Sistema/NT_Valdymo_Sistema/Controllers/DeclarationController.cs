﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NT_Valdymo_Sistema.Models;
namespace NT_Valdymo_Sistema.Controllers
{
    public class DeclarationController : Controller
    {
        private REDatabaseEntities db = new REDatabaseEntities();

        // GET: Declaration
        public ActionResult Declaration()
        {
            return View();
        }
        public ActionResult About()
        {
            ViewBag.Message = "Nekilnojamojo turto valdymo sistema";

            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Declaration(House house, Declaration declaration)
        {
            bool Status = false;
            string message = "";

            // Modelio validacija

            if (ModelState.IsValid)
            {
                #region // Išsaugojimas į duomenų bazę
                using (REDatabaseEntities dc = new REDatabaseEntities())
                {
                    declaration.DeclarationDate = System.DateTime.Now;
                    var userID = Session["userID"];
                    declaration.UserID = Convert.ToInt32(userID);
                    declaration.HouseID = house.HouseID;
                    house.Declarated = 1;
                    dc.Declaration.Add(declaration);
                    dc.House.Add(house);
                    dc.SaveChanges();

                    Status = true;
                    return RedirectToAction("DeclarationInfo", "Declaration");
                }
                #endregion
            }
            else
            {
                message = "Neteisinga užklausa";
            }

            ViewBag.Message = message;
            ViewBag.Status = Status;
            return View(house);
        }

        //Atidaryti posistemes pasirinkimo langa
        public ActionResult Subsystem()
        {
            return View();
        }

        //Atidaryti deklaravimo posistemes pagrindini langa
        public ActionResult DeclarationMain()
        {
            return View();
        }

        //pasirinkti deklaruojamo NT tipa
        public ActionResult DeclarationType()
        {
            return View();
        }

        public ActionResult DeclarationInfo()
        {
            using (REDatabaseEntities dc = new REDatabaseEntities())
            {
                var declarations = new List<Declaration>();
                var houses = new List<House>();

                var userID = Convert.ToInt32(Session["userID"]);

                declarations = dc.Declaration.Where(a => a.UserID == userID).ToList();
                foreach (var d in declarations)
                {
                    houses.Add(dc.House.Where(a => a.HouseID == d.HouseID).First());
                }
                ViewBag.Houses = houses;
                return View(declarations);
            }
        }

        public ActionResult DetailedInfo(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.BadRequest);
            }
            var declaration = db.Declaration.Find(id);
            if (declaration == null)
            {
                return HttpNotFound();
            }
            return View(declaration);
        }
    }
}