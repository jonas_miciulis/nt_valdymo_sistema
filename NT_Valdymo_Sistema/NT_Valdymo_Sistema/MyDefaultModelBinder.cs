﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.ModelBinding;
using System.Web.Mvc;
using NT_Valdymo_Sistema.Models;
namespace NT_Valdymo_Sistema
{
    public class MyDefaultModelBinder : System.Web.Mvc.DefaultModelBinder
    {
        protected override object CreateModel(ControllerContext controllerContext, System.Web.Mvc.ModelBindingContext bindingContext, Type modelType)
        {
            if (modelType == typeof(Tuple<Advertisement,House>))
                return new Tuple<Advertisement, House>(new Advertisement(), new House());
            return base.CreateModel(controllerContext, bindingContext, modelType);
        }
    }
}