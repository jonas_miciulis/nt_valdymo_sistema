﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace NT_Valdymo_Sistema.Models
{
    [MetadataType(typeof(UserMetadata))]
    public partial class User
    {
        public string ConfirmPassword { get; set; }
    }

    public class UserMetadata
    {
        [Display(Name = "Vardas")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Įveskite savo vardą")]
        public string Name { get; set; }

        [Display(Name = "Pavardė")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Įveskite savo pavardę")]
        public string Surname { get; set; }

        [Display(Name = "El. paštas")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Įveskite el. paštą")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Display(Name = "Miestas")]
        public string City { get; set; }

        [Display(Name = "Adresas")]
        public string Address { get; set; }

        [Display(Name = "Slaptažodis")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Įveskite slaptažodį")]
        [DataType(DataType.Password)]
        [MinLength(6, ErrorMessage = "Slaptažodį turi sudaryti bent 6 simboliai")]
        public string Password { get; set; }


        [Display(Name = "Pakartokite slaptažodį")]
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Pakartotinis slaptažodis nesutampa su įvestu slaptažodžiu")]
        public string ConfirmPassword { get; set; }
    }
}