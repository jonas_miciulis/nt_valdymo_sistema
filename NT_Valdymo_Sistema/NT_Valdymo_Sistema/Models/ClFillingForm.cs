﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace NT_Valdymo_Sistema.Models
{
    public class ClFillingForm
    {
        [Display(Name = "Nurodykite NT tipą")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Pasirinkite NT tipą")]
        public string selectType { get; set; }

        [Display(Name = "Nurodykite turtą")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Pasirinkite turtą")]
        public string selectDeclaration { get; set; }

        [Display(Name = "Siųsti žinutę į el paštą, kai vertinimas bus atliktas")]
        public bool sendeMessageToEmail { get; set; }
    }
}