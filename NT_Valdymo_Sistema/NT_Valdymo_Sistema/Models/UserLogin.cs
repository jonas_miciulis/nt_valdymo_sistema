﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace NT_Valdymo_Sistema.Models
{
    public class UserLogin
    {
        [Display(Name = "El. paštas")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Įveskite el. paštą")]
        public string email { get; set; }

        [Display(Name = "Slaptažodis")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Įveskite slaptažodį")]
        [DataType(DataType.Password)]
        public string password { get; set; }

        [Display(Name = "Įsiminti")]
        public bool rememberMe { get; set; }
    }
}